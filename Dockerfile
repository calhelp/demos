FROM ubuntu:bionic

# Install LEMP
RUN \ 
    apt-get update && \
    export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true && \
    apt-get install -y curl net-tools git zip unzip \
                       php php-zip php-mbstring php-dom php-fpm php-mysql nginx \
                       mysql-server mysql-client openjdk-8-jre-headless
RUN mkdir -p /app
COPY ./configs /app/configs
WORKDIR /app
# Update mysqld.conf
RUN \
    rm -f /etc/nginx/sites-available/default && cp /app/configs/nginx.default /etc/nginx/sites-available/default && \
    rm -f /etc/mysql/mysql.conf.d/mysqld.cnf && cp /app/configs/my.cnf /etc/mysql/mysql.conf.d/mysqld.cnf && \
    rm -f /etc/php/7.2/fpm/php.ini && cp /app/configs/php.ini /etc/php/7.2/fpm/php.ini

#RUN \
#    git clone https://github.com/yiisoft/yii && \
#    cd yii/framework && ./yiic webapp ../public

# Install Yii2
#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
#    /usr/local/bin/composer global require "fxp/composer-asset-plugin"

#RUN /usr/local/bin/composer create-project --prefer-dist \
#    yiisoft/yii2-app-basic /app/yii2
# Clone yii1 and create files at /app/yii/public
COPY ./yii /app/yii


# Make run.sh script
RUN mkdir -p /run/php/ && mkdir -p /app/yii/public/protected/runtime && mkdir -p /app/yii/public/assets
RUN chown www-data.www-data -R /app/yii

RUN \
    echo '#!/bin/bash \n \
    service php7.2-fpm start \n\
    service nginx start \n \
    tail -f -n200 /var/log/nginx/access.log' > /app/run.sh

RUN chmod +x /app/run.sh

EXPOSE 80
ENTRYPOINT ["/app/run.sh"]
